require 'rubygems'
require 'mechanize'

a = Mechanize.new { |agent|
  agent.user_agent_alias = 'Mac Safari'
}

a.get('http://www.bountyway.com/') do |page|
  search_result = a.click(page.link_with(text: /your assets free/))
   
  search_result.links.each do |link|
    puts link.text
  end
end